package com.company;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

    private static int comparison=0;
    private static Random random;
    static List<Integer> array;
    static int k;
    static boolean founded=false;

    public static void main(String[] args) {
        random= new Random(Instant.now().toEpochMilli());
         array= new ArrayList<>();
         k=random.nextInt(10);
        for(int i=0;i<10;i++){
            int tmp=random.nextInt(1000);
            while(array.contains(tmp)){
                tmp=random.nextInt(1000);
            }
            array.add(tmp);
            //array.add(i);
        }
        Collections.shuffle(array,random);
        quickSelect(0,array.size()-1);
    }

    private static void quickSelect(int from, int to){


        while(!founded) {
            int pivot = random.nextInt((to - from) + 1) + from;

            for (int i = from; i < pivot; i++) {
                comparison++;
                if (array.get(i) > array.get(pivot)) {
                    array.add(pivot+1,array.get(i));
                    array.remove(i);
                    i--;
                    pivot--;
                }
            }

            for (int i = pivot + 1; i <= to; i++) {
                comparison++;
                if (array.get(i) < array.get(pivot)) {
                    array.add(pivot, array.get(i));
                    pivot++;
                    array.remove(i + 1);

                }
            }

             if(k<pivot) {
                 //quickSelect(from, pivot - 1);
                 to=pivot-1;
             }else if(k>pivot) {
                 //quickSelect(pivot + 1, to);
                 from=pivot+1;
             }
            if (k == pivot ) {//else {
                founded = true;
                System.out.println(array.toString());
                System.out.println("k=" + k + " and pivot=" + pivot + " comparaison:" + comparison);
            }
        }

    }
}
